#include <DHT.h>


#include <LiquidCrystal_I2C.h>

#include <LiquidCrystal.h>
LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display


int sensor_pin = A3;
int output_value ;
int motorPin = 3;  
int watertime = 5;
#include "DHT.h"
#include "SoftwareSerial.h"
SoftwareSerial SER (10,11);//(rx,tx)

DHT dht(9, DHT22);

#define s0 4
#define s1 5
#define s2 7
#define s3 6
#define sensorOut 8

int redFrequency = 0;
int greenFrequency = 0;
int blueFrequency = 0;

int redColor = 0;
int blueColor = 0;
int greenColor = 0;

void setup() {
// put your setup code here, to run once:
pinMode(s0,OUTPUT);
pinMode(s1,OUTPUT);
pinMode(s2,OUTPUT);
pinMode(s3,OUTPUT);

pinMode(sensorOut,INPUT);
dht.begin();

digitalWrite(s0,HIGH);
digitalWrite(s1,LOW);
pinMode(motorPin, OUTPUT);
Serial.begin(9600);
SER.begin(9600);
lcd.init();
lcd.clear();         
lcd.backlight();      // Make sure backlight is on

lcd.clear();
 lcd.setCursor(0,0);   //Set cursor to character 2 on line 0
  lcd.print("Waiting for ESP01");
 lcd.setCursor(0,1);
  lcd.print("connection!");
//while(SER.read()!='g');
lcd.clear();
 lcd.setCursor(0,0);   //Set cursor to character 2 on line 0
  lcd.print("Connected");
  delay(3000);
  
}

void loop() {
// put your main code here, to run repeatedly:
float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
 float t = dht.readTemperature();
//reading red filter
digitalWrite(s2,LOW);
digitalWrite(s3,LOW);

redFrequency = pulseIn(sensorOut, LOW);
Serial.print("R = ");
Serial.print(redFrequency);
delay(100);

redColor = map(redFrequency, 63, 218, 255,0);

//reading green filter
digitalWrite(s2,HIGH);
digitalWrite(s3,HIGH);

greenFrequency = pulseIn(sensorOut,LOW);
Serial.print(" G = ");
Serial.print(greenFrequency);
delay(100);

greenColor = map(greenFrequency, 54, 144, 255,0);


//reading blue
digitalWrite(s2,LOW);
digitalWrite(s3,HIGH);

blueFrequency = pulseIn(sensorOut,LOW);
Serial.print(" B = ");
Serial.println(blueFrequency);
delay(100);

blueColor = map(blueFrequency, 41, 190, 255,0);


// put your main code here, to run repeatedly:
output_value= analogRead(sensor_pin);
output_value = map(output_value,394,674,100,0);
Serial.print("Mositure : ");
Serial.println(output_value);
Serial.print("%");
if(output_value<25)
{
digitalWrite(motorPin, HIGH);
delay(50000);
digitalWrite(motorPin, LOW);
}
else{
 digitalWrite(motorPin, LOW);
}
String Send=String(h)+"/"+String(t)+"/"+String(output_value)+"/"+String((blueColor+redColor+greenColor)/3)+"*";
for (int i=0;i<Send.length()+1;i++)
{
 SER.write(Send[i]);
 delay(1);
  }    

//..delay(3000);
lcd.clear();
lcd.setCursor(0,0);
lcd.print("T: ");
lcd.setCursor(3,0);
lcd.print((int)t);

lcd.setCursor(6,0);
lcd.print("H:");
lcd.setCursor(9,0);
lcd.print((int)h);
lcd.setCursor(0,1);
lcd.print("M:");
lcd.setCursor(3,1);
lcd.print((int)output_value);
lcd.setCursor(6,1);
lcd.print("L:");
lcd.setCursor(9,1);
lcd.print((blueColor+redColor+greenColor)/3);

}
