# NPM - NODEJS - PUG - COREUI - MYSQL

For this Project its split into 3 directories
1. FULL
2. esp
3. arduino-web
 
 1. the Full directory contains the arduino code that runs the entire board. 
 this code has already been uploaded to the board.
 
 2. the esp directory contains the code that configures the esp8266 module and connects it to the web API
 
 3. arduino-web folder contains the web API code. 
 to be able to run the code NPM should be installed on the system 
        "npm install"
        
       " npm run dev"
       
       

ngrok was used to be able to make the local web server running on my machine be acessible globally by giving it the port its running on. 
The Global link to access the web page is :

"  https://8ea651531266.ngrok.io "

app.js : main project file includes :
1. API 
2. project logic 
3. sql queries 

pug directory : includes the project views

Public -> assets - > css : contains the coreUI for charts design and user interface
