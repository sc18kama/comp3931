const express = require("express");
const path = require("path");
const port = process.env.PORT || "8000";
const session = require('express-session');
const connection = require('./conn/mySql');
const datetime = require("node-datetime");
const moment = require("moment");
const app = express();
app.locals.basedir = path.join(__dirname, 'pug');
app.use(express.static(path.join(__dirname, "public")));
app.set("views", path.join(__dirname, "pug"));
app.set("view engine", "pug");


app.use(express.json({
  limit: '10mb'
}));
app.use(express.urlencoded({
  limit: '10mb',
  extended: true
}));
app.use(session({secret: 'arduino'}));
app.use(session({
  genid: function (req) {
    return genuuid() // use UUIDs for session IDs
  },
  secret: 'arduino',
  resave: false,
  saveUninitialized: true
}))
app.get("/", async (req, res) => {
  if (req.session.user != undefined ) {

    let data = {
      title: "Dashboard",
      todayDate: datetime.create().format('Y-m-d'),
      last7Days: []
    }
    console.log("Start");

    let current = {
      humidity: {
        value: 0,
        updated_on: " -_-_- "
      },
      temperature: {
        value: 0,
        updated_on: " -_-_- "
      },
      moisture: {
        value: 0,
        updated_on: " -_-_- "
      }
    }

    let temperature = await SelectAllElements("SELECT temperature,added_on from arduino_readings WHERE temperature !='' ORDER BY id DESC LIMIT 1")
    if (temperature.length > 0) {
      current.temperature = {
        value: temperature[0]['temperature'] || 0,
        updated_on: temperature[0]["added_on"] == "" ? ' --- ' : moment(datetime.create(temperature[0]["added_on"]).format('Y-m-d H:M:S')).fromNow(),
      }
    }
    let moisture = await SelectAllElements("SELECT moisture,added_on from arduino_readings WHERE moisture !='' ORDER BY id DESC LIMIT 1")
    if (moisture.length > 0) {
      current.moisture = {
        value: moisture[0]['moisture'] || 0,
        updated_on: moisture[0]["added_on"] == "" ? ' --- ' : moment(datetime.create(moisture[0]["added_on"]).format('Y-m-d H:M:S')).fromNow(),
      }
    }
    let humidity = await SelectAllElements("SELECT humidity,added_on from arduino_readings WHERE humidity !='' ORDER BY id DESC LIMIT 1")
    if (humidity.length > 0) {
      current.humidity = {
        value: humidity[0]['humidity'] || 0,
        updated_on: humidity[0]["added_on"] == "" ? ' --- ' : moment(datetime.create(humidity[0]["added_on"]).format('Y-m-d H:M:S')).fromNow(),
      }
    }
    data.currentValues = current;

    let chartDataToday = {
      xAxis: [],
      humidity: [],
      temperature: [],
      moisture: [],
      color: [],
    }
    for (let i = 0; i <= new Date().getHours(); i++) {
      chartDataToday.xAxis.push(`${data.todayDate} ${padLeadingZeros(i, 2)}`)
      chartDataToday.humidity.push(0);
      chartDataToday.temperature.push(0);
      chartDataToday.moisture.push(0);
      chartDataToday.color.push(0);
    }

    let todayByTime = await SelectAllElements("SELECT \n" +
      "    DATE_FORMAT(added_on, '%Y-%m-%d %H') AS Date,\n" +
      "    SUM(temperature) AS temperature,\n" +
      "    SUM(moisture) AS moisture,\n" +
      "    SUM(humidity) AS humidity,\n" +
      "    SUM(color) AS color\n" +
      "FROM\n" +
      "    arduino_readings\n" +
      "WHERE added_on between '" + data.todayDate + " 00:00:00' and '" + data.todayDate + " 23:59:59'\n" +
      "GROUP BY (1)\n" +
      "ORDER BY Date ASC;")


    if (todayByTime.length > 0) {
      for (let i = 0; i < todayByTime.length; i++) {
        const row = todayByTime[i],
          iDx = chartDataToday.xAxis.indexOf(row['Date']);

        if (iDx != -1) {
          if (row['moisture'] != null && parseInt(row['moisture']) > 0) {
            chartDataToday.moisture[iDx] = parseInt(row['moisture']);
          }
          if (row['temperature'] != null && parseInt(row['temperature']) > 0) {
            chartDataToday.temperature[iDx] = parseInt(row['temperature']);
          }
          if (row['humdity'] != null && parseInt(row['humdity']) > 0) {
            chartDataToday.humdity[iDx] = parseInt(row['humdity']);
          }
          if (row['color'] != null && parseInt(row['color']) > 0) {
            chartDataToday.color[iDx] = parseInt(row['color']);
          }
        }
      }
    }
    data.chartDataToday = chartDataToday;

    let last7DaysData = await SelectAllElements("SELECT \n" +
      "   concat( DAYNAME(DATE_FORMAT(added_on, '%Y-%m-%d')),\" \",DATE_FORMAT(added_on, '%M-%d')) AS Date,\n" +
      "   DATE_FORMAT(added_on, '%Y-%m-%d') AS FullDate,\n" +
      "    SUM(temperature) AS temperature,\n" +
      "    SUM(moisture) AS moisture,\n" +
      "    SUM(humidity) AS humidity,\n" +
      "    SUM(color) AS color,\n" +
      "    IF(SUM(color)IS NULL ,0,SUM(color)) + IF(SUM(humidity)IS NULL ,0,SUM(humidity)) + IF(SUM(moisture)IS NULL ,0,SUM(moisture)) + IF(SUM(temperature)IS NULL ,0,SUM(temperature)) AS total\n" +
      "FROM\n" +
      "    arduino_readings\n" +
      "WHERE\n" +
      "    added_on > NOW() - INTERVAL 1 WEEK\n" +
      "GROUP BY Date\n" +
      "ORDER BY FullDate DESC;")
    //console.log(moment("2021-05-04 00:58:38").fromNow());
    if (last7DaysData.length > 0) {
      for (let i = 0; i < last7DaysData.length; i++) {
        data.last7Days.push(last7DaysData[i])
      }
    }
    console.log("end");
    res.render("index", data);
  } else {
    res.redirect('/login');
  }

});
app.get("/login", (req, res) => {
  if (req.session.user != undefined) {
    res.redirect('/');
  } else {
    res.render("login", {title: "Login"});
  }
});
app.get("/register", (req, res) => {
  if (req.session.user) {
    res.redirect('/login');
  } else {
    res.render("register", {title: "Register"});
  }
});
app.post("/login-check", (req, res) => {
  if (req.body.email == undefined || req.body.email == "") {
    return res.status(200).json({
      'status': 'failed',
      'message': 'Invalid email address provided'
    });
  } else if (req.body.password == undefined || req.body.password == "") {
    return res.status(200).json({
      'status': 'failed',
      'message': 'Invalid password provided'
    });
  } else {
    connection.query(`SELECT * FROM users where email = '${req.body.email}' AND password =  '${req.body.password}' LIMIT 1`, function (error, results, fields) {
      if (error) {
        return res.status(200).json({
          'status': 'failed',
          'message': 'Invalid username or password',
        });
      }
      if (results.length > 0) {
        req.session.user = results[0]['id'];
        return res.status(200).json({
          'status': 'success',
          'message': 'Account verified',
        });
      }
      return res.status(200).json({
        'status': 'failed',
        'message': 'Invalid username or password'
      });
    });
  }

});

SelectAllElements = async (query) => {
  return new Promise((resolve, reject) => {
    connection.query(query, (error, elements) => {
      if (error) {
        return reject(error);
      }
      return resolve(elements);
    });
  });
};

function padLeadingZeros(num, size) {
  var s = num + "";
  while (s.length < size) s = "0" + s;
  return s;
}

app.post("/signup", (req, res) => {
  if (req.body.email == undefined || req.body.email == "") {
    return res.status(200).json({
      'status': 'failed',
      'message': 'Invalid email address provided'
    });
  } else if (req.body.pass == undefined || req.body.pass == "") {
    return res.status(200).json({
      'status': 'failed',
      'message': 'Invalid password provided'
    });
  } else {
    connection.query(`INSERT INTO \`arduino\`.\`users\` (\`email\`, \`password\`, \`created_date\`) VALUES ( '${req.body.email}','${req.body.pass}','${datetime.create().format('Y-m-d H:M:S')}' ) `, function (error, results, fields) {
      if (error) {
        return res.status(200).json({
          'status': 'failed',
          'message': 'Failed to create account',
          'res': JSON.stringify(error)
        });
      }

      if (results.affectedRows > 0) {
        return res.status(200).json({
          'status': 'success',
          'message': 'Successfully created',

        });
      }
      return res.status(200).json({
        'status': 'failed',
        'message': 'Failed to create account',

      });
    });
  }

});

app.get("/save_readings", (req, res) => {
  try {

    let temp = req.query.temp || null,
      moisture = req.query.mos || null,
      color = req.query.color || null,
      hum = req.query.hum || null;
    connection.query(`INSERT INTO \`arduino\`.\`arduino_readings\` (\`temperature\`, \`moisture\`, \`humidity\`, \`color\`, \`added_on\`, \`timestamp\`) VALUES
     ( '${temp}','${moisture}','${hum}','${color}','${datetime.create().format('Y-m-d H:M:S')}' , ${Date.now()} ) `, function (error, results, fields) {
      if (error) {
        return res.status(200).json({
          'status': 'failed',
          'message': 'Failed to save reading',
          'res': JSON.stringify(error)
        });
      }

      if (results.affectedRows > 0) {
        return res.status(201).json({
          'status': 'success',
          'message': 'Successfully saved',

        });
      }
      return res.status(403).json({
        'status': 'failed',
        'message': 'Failed to save reading',

      });
    });

  } catch (e) {
    return res.status(400).json({
      'status': 'failed',
      'message': e.message
    });
  }
});
app.get("/logout", (req, res) => {
  delete req.session.user;
  res.redirect('/');
});


app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});
