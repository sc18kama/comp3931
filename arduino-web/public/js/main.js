/* eslint-disable object-shorthand */
/* global Chart, coreui, coreui.Utils.getStyle, coreui.Utils.hexToRgba */

/**
 * --------------------------------------------------------------------------
 * CoreUI Boostrap Admin Template (v3.2.0): main.js
 * Licensed under MIT (https://coreui.io/license)
 * --------------------------------------------------------------------------
 */

/* eslint-disable no-magic-numbers */
// Disable the on-canvas tooltip
Chart.defaults.global.pointHitDetectionRadius = 1
Chart.defaults.global.tooltips.enabled = false
Chart.defaults.global.tooltips.mode = 'index'
Chart.defaults.global.tooltips.position = 'nearest'
Chart.defaults.global.tooltips.custom = coreui.ChartJS.customTooltips
Chart.defaults.global.defaultFontColor = '#646470'
Chart.defaults.global.responsiveAnimationDuration = 1
document.body.addEventListener('classtoggle', event => {
  if (event.detail.className === 'c-dark-theme') {
    if (document.body.classList.contains('c-dark-theme')) {
      cardChart1.data.datasets[0].pointBackgroundColor = coreui.Utils.getStyle('--primary-dark-theme')
      cardChart2.data.datasets[0].pointBackgroundColor = coreui.Utils.getStyle('--info-dark-theme')
      Chart.defaults.global.defaultFontColor = '#fff'
    } else {
      cardChart1.data.datasets[0].pointBackgroundColor = coreui.Utils.getStyle('--primary')
      cardChart2.data.datasets[0].pointBackgroundColor = coreui.Utils.getStyle('--info')
      Chart.defaults.global.defaultFontColor = '#646470'
    }

    cardChart1.update()
    cardChart2.update()
    mainChart.update()
  }
})

// eslint-disable-next-line no-unused-vars
if (document.getElementById('main-chart')) {
  /*const mainChart = new Chart(document.getElementById('main-chart'), {
    type: 'line',
    data: {
      labels: ['M'],
      datasets: [
        {
          label: 'My First dataset',
          backgroundColor: coreui.Utils.hexToRgba(coreui.Utils.getStyle('--info'), 10),
          borderColor: coreui.Utils.getStyle('--info'),
          pointHoverBackgroundColor: '#fff',
          borderWidth: 2,
          data: [35]
        },
        {
          label: 'My Second dataset',
          backgroundColor: 'transparent',
          borderColor: coreui.Utils.getStyle('--success'),
          pointHoverBackgroundColor: '#fff',
          borderWidth: 2,
          data: [35]        },
        {
          label: 'My Third dataset',
          backgroundColor: 'transparent',
          borderColor: coreui.Utils.getStyle('--danger'),
          pointHoverBackgroundColor: '#fff',
          borderWidth: 1,
          borderDash: [8, 5],
          data: [35]        }
      ]
    },
    options: {
      maintainAspectRatio: false,
      legend: {
        display: true
      },
      scales: {
        xAxes: [{
          gridLines: {
            drawOnChartArea: true
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true,
            maxTicksLimit: 5,
            // stepSize: Math.ceil(250 / 5),
            // max: 250
          }
        }]
      },
      elements: {
        point: {
          radius: 0,
          hitRadius: 10,
          hoverRadius: 4,
          hoverBorderWidth: 3
        }
      }
    }
  })*/
  function drawChart(data) {
    data = data.chartDataToday || {}
    console.log(data)
    const mainChart = new Chart(document.getElementById('main-chart'), {
      type: 'line',
      data: {
          labels: data.xAxis,
        datasets: [
          {
            label: 'Temperature',
            backgroundColor: coreui.Utils.hexToRgba(coreui.Utils.getStyle('--info'), 10),
            borderColor: coreui.Utils.getStyle('--info'),
            pointHoverBackgroundColor: '#fff',
            borderWidth: 2,
            data: data.temperature

          },
          {
            label: 'Moisture',
            backgroundColor: 'transparent',
            borderColor: coreui.Utils.getStyle('--success'),
            pointHoverBackgroundColor: '#fff',
            borderWidth: 2,
            data: data.moisture
          },
          {
            label: 'Humdity',
            backgroundColor: 'transparent',
            borderColor: coreui.Utils.getStyle('--danger'),
            pointHoverBackgroundColor: '#fff',
            borderWidth: 1,
            borderDash: [8, 5],
            data: data.humidity
          },
          {
            label: 'Color',
            backgroundColor: 'transparent',
            borderColor: coreui.Utils.getStyle('--primary'),
            pointHoverBackgroundColor: '#fff',
            borderWidth: 1,
            borderDash: [8, 5],
            data: data.color
          }
        ]
      },
      options: {
        maintainAspectRatio: false,
        legend: {
          display: true
        },
        scales: {
          xAxes: [{
            gridLines: {
              drawOnChartArea: true
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              maxTicksLimit: 5,
              // stepSize: Math.ceil(250 / 5),
              // max: 250
            }
          }]
        },
        elements: {
          point: {
            radius: 0,
            hitRadius: 10,
            hoverRadius: 4,
            hoverBorderWidth: 3
          }
        }
      }
    })
  }
}



$(document).ready(()=>{
  //$('[data-toggle="tooltip"]').tooltip();
})
