console.log('App Loaded');
const loginBtn = document.getElementById("btnLogin");
if (loginBtn) {
  loginBtn.addEventListener('click', () => {
    const email = document.getElementById("txtEmail"),
      password = document.getElementById("txtPassword");
    if (email == null || email.value == "") {
      $.toast({
        heading: 'Validation',
        text: 'Please enter valid email',
        icon: 'danger',
        loader: true,        // Change it to false to disable loader
        loaderBg: '#c11f1f',  // To change the background
        position: 'top-right',
        stack: false
      })
    }
    if (password == null || password.value == "") {
      $.toast({
        heading: 'Validation',
        text: 'Please enter valid password',
        icon: 'danger',
        loader: true,        // Change it to false to disable loader
        loaderBg: '#c11f1f',  // To change the background
        position: 'top-right',
        stack: false
      })
    } else {
      $("#loading").show();
      $.ajax({
        url: '/login-check',
        data: {
          email: email.value,
          password: password.value
        },
        method: 'post',
        success: (response) => {
          $("#loading").hide();
          if (response.status == "success") {
            window.location = '/';
          } else if (response.status == "failed") {
            $.toast({
              heading: 'Error',
              text: response.message,
              icon: 'danger',
              loader: true,        // Change it to false to disable loader
              loaderBg: '#c11f1f',  // To change the background
              position: 'top-right',
              stack: false
            })
          }
        }
      });
    }
  })
}
const registerBtn = document.getElementById("btnCreateAcc");
if (registerBtn) {
  registerBtn.addEventListener('click', () => {
    const email = $("#txtEmail").val(),
      pass = $("#txtPass").val(),
      cpass = $("#txtPassR").val();
    if (email == undefined || email == "") {
      $.toast({
        heading: 'Validation',
        text: 'Please enter valid email',
        icon: 'danger',
        loader: true,        // Change it to false to disable loader
        loaderBg: '#c11f1f',  // To change the background
        position: 'top-right',
        stack: false
      })
    } else if (!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)) {
      $.toast({
        heading: 'Validation',
        text: 'Email is not in proper format',
        icon: 'danger',
        loader: true,        // Change it to false to disable loader
        loaderBg: '#c11f1f',  // To change the background
        position: 'top-right',
        stack: false
      })
    } else if (pass == undefined || pass == "") {
      $.toast({
        heading: 'Validation',
        text: 'Please enter valid password',
        icon: 'danger',
        loader: true,        // Change it to false to disable loader
        loaderBg: '#c11f1f',  // To change the background
        position: 'top-right',
        stack: false
      })
    } else if (cpass == undefined || cpass == "" || pass !== cpass) {
      $.toast({
        heading: 'Validation',
        text: 'Password donot match',
        icon: 'danger',
        loader: true,        // Change it to false to disable loader
        loaderBg: '#c11f1f',  // To change the background
        position: 'top-right',
        stack: false
      })
    } else {
      $("#loading").show();
      $.ajax({
        url: '/signup',
        data: {
          email: email,
          pass: pass
        },
        method: 'post',
        success: (response) => {
          $("#loading").hide();
          if (response.status == "success") {
            setTimeout(() => {
              $.toast({
                heading: 'Success',
                text: response.message,
                icon: 'Info',
                loader: true,        // Change it to false to disable loader
                loaderBg: '#12bf00',  // To change the background
                position: 'top-right',
                stack: false
              });

            }, 2000)
            window.location = 'login';
          } else if (response.status == "failed") {
            $.toast({
              heading: 'Error',
              text: response.message,
              icon: 'danger',
              loader: true,        // Change it to false to disable loader
              loaderBg: '#c11f1f',  // To change the background
              position: 'top-right',
              stack: false
            })
          }
        }
      });
    }

  })
}
