const mysqlClient = require('mysql');
let mySqlCon = mysqlClient.createConnection({
  host: '127.0.0.1',
  user: 'root',
  password: 'root',
  database: 'arduino'
});
mySqlCon.connect(function (err) {
  console.log('Connection MySQL Server');
  if (err) {
    console.log({
      message: 'MySQL Connection Error: ' + JSON.stringify(err, null, '\t'),
      level: 'error'
    });
    mySqlCon = mySQLReconnect(mySqlCon);
  } else {
    console.log({
      message: 'MySQL Connection Created',
      level: 'info'
    });
  }
});

mySqlCon.on('error', function (err) {
  if (err.code === "PROTOCOL_CONNECTION_LOST") {
    console.log({
      message: 'MySQL Connection Error 4: ' + JSON.stringify(err.message, null, '\t'),
      level: 'error'
    });
    mySqlCon = mySQLReconnect(mySqlCon);
  } else if (err.code === "PROTOCOL_ENQUEUE_AFTER_QUIT") {
    console.log({
      message: 'MySQL Connection Error 3: ' + JSON.stringify(err, null, '\t'),
      level: 'error'
    });
    mySqlCon = mySQLReconnect(mySqlCon);
  } else if (err.code === "PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR") {
    console.log({
      message: 'MySQL Connection Error 2: ' + JSON.stringify(err, null, '\t'),
      level: 'error'
    });
    mySqlCon = mySQLReconnect(mySqlCon);
  } else if (err.code === "PROTOCOL_ENQUEUE_HANDSHAKE_TWICE") {
    console.log({
      message: 'MySQL Connection Error 1: ' + JSON.stringify(err, null, '\t'),
      level: 'error'
    });
  } else {
    console.log({
      message: 'MySQL Connection Error 5: ' + JSON.stringify(err, null, '\t'),
      level: 'error'
    });
    mySqlCon = mySQLReconnect(mySqlCon);
  }
});

function mySQLReconnect(connection) {
  console.log("Reconnect");

  if (connection) {
    connection.destroy();
  }
  var mySqlConRe = mysqlClient.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: 'root',
    database: 'arduino'
  });
  mySqlConRe.connect(function (err) {
    if (err) {
      setTimeout(mySQLReconnect, 2000);
    } else {
      return mySqlConRe;
    }
  });
}

module.exports = mySqlCon;
